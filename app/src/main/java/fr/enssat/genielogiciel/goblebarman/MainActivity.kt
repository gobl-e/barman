package fr.enssat.genielogiciel.goblebarman

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import fr.enssat.genielogiciel.goblebarman.Utils.Companion.getErrorMessage
import fr.enssat.genielogiciel.goblebarman.Utils.Companion.hideKeyboard
import fr.enssat.genielogiciel.goblebarman.databinding.ActivityMainBinding
import org.json.JSONException
import org.json.JSONObject


class MainActivity : Activity() {
    private lateinit var binding: ActivityMainBinding
    private var LoginURL = "https://api.move-it.tech/login"
    private var preferenceHelper: PreferenceHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        preferenceHelper = PreferenceHelper(this)

        if (preferenceHelper!!.getIsLogin()) {
            val intent = Intent(this@MainActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }

        binding.btnLogin.setOnClickListener {
            login()
            hideKeyboard()
        }
    }

    private fun login() {
        try {
            Fuel.post(
                LoginURL, listOf(
                    "email" to binding.textEmail.text.toString()
                    , "password" to binding.textMdp.text.toString()
                    , "admin" to 1
                )
            ).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            422 -> handleError(String(response.data))
                            401 -> Toast.makeText(
                                this@MainActivity,
                                "Unknown login and/or password.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        onTaskCompleted(result.get().content)
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@MainActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun onTaskCompleted(response: String) {
        val jsonObject = JSONObject(response)

        if (jsonObject.getString("message") == "LOGGED") {
            preferenceHelper!!.putIsLogin(true)
            try {
                preferenceHelper!!.putToken(jsonObject.getString("token"))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val intent = Intent(this@MainActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        } else {
            Toast.makeText(this@MainActivity, getErrorMessage(response), Toast.LENGTH_SHORT).show()
        }
    }

    private fun handleError(response: String) {
        val jsonObject = JSONObject(response)
        val keys = jsonObject.keys()

        while (keys.hasNext()) {
            val key = keys.next()
            val message = jsonObject.optJSONArray(key)[0].toString()

            when (key) {
                "email" -> binding.textEmail.error = message
                "password" -> binding.textMdp.error = message
            }
        }
    }

}
