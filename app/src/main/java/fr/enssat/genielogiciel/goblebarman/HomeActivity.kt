package fr.enssat.genielogiciel.goblebarman

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeRecyclerView
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import fr.enssat.genielogiciel.goblebarman.Utils.Companion.hideKeyboard
import fr.enssat.genielogiciel.goblebarman.data.model.Drink
import fr.enssat.genielogiciel.goblebarman.data.model.DrinkViewModel
import fr.enssat.genielogiciel.goblebarman.databinding.ActivityHomeBinding
import kotlinx.android.synthetic.main.activity_home.*
import org.json.JSONObject


class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private var DrinksURL = "https://api.move-it.tech/boisson"
    private var CustomerProfile = "https://api.move-it.tech/user/profile"
    private var ProfileURL = "https://api.move-it.tech/admin/profile"
    private var BalanceURL = "https://api.move-it.tech/user/{id}/balance"
    private var preferenceHelper: PreferenceHelper? = null
    private var token: String = ""
    private var userId: String = ""
    private var solde: Double = 0.0
    private var model: DrinkViewModel? = null
    private var adapter: DrinkListAdapter = DrinkListAdapter(emptyList(), this::addToSum)
    private var adapterSum = DrinkSumAdapter(this::removeFromSum)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        preferenceHelper = PreferenceHelper(this)
        token = preferenceHelper!!.getToken() ?: ""

        if (token == "") {
            logout(true)
        }

        val actionBar: ActionBar? = supportActionBar
        actionBar!!.hide()

        getProfile()

        getDrinks()

        model = ViewModelProvider(this).get(DrinkViewModel::class.java)
        binding.dragDropList.layoutManager = GridLayoutManager(this, 3)
        binding.dragDropList.adapter = adapter
        binding.dragDropList.orientation =
            DragDropSwipeRecyclerView.ListOrientation.GRID_LIST_WITH_HORIZONTAL_SWIPING
        binding.dragDropList.numOfColumnsPerRowInGridList = 3
        binding.dragDropList.disableSwipeDirection(DragDropSwipeRecyclerView.ListOrientation.DirectionFlag.RIGHT)
        binding.dragDropList.disableSwipeDirection(DragDropSwipeRecyclerView.ListOrientation.DirectionFlag.LEFT)

        binding.sumOfDrinks.adapter = adapterSum

        model!!.drinks.observe(this, Observer<List<Drink>> { list ->
            adapterSum.list = list
        })


        binding.btnReadToken.setOnClickListener {
            getUserBalance()
            hideKeyboard()
        }

        binding.btnConfirm.setOnClickListener {
            pay()
        }

        binding.btnCancel.setOnClickListener {
            clearSum()
        }

        binding.btnLogout.setOnClickListener {
            logout(false)
        }

        binding.btnSetup.setOnClickListener {
            val intent = Intent(this@HomeActivity, SetupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }

    }

    private fun logout(auto: Boolean) {
        if (auto) {
            Toast.makeText(
                this@HomeActivity,
                "Session expirée, veuillez vous reconnecter",
                Toast.LENGTH_SHORT
            ).show()
        }
        preferenceHelper!!.putIsLogin(false)
        val intent = Intent(this@HomeActivity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        this@HomeActivity.finish()
    }

    private fun addToSum(d: Drink) {
        model!!.add(d)
        updateTotal()
    }

    private fun removeFromSum(d: Drink) {
        model!!.removeSum(d)
        updateTotal()
    }

    private fun updateTotal() {
        binding.balanceLeft.text = balanceLeft!!.getContext()
            .getString(R.string.balance_left, solde - model!!.getTotal())

        if (model!!.isEmpty()) {
            binding.group.visibility = View.INVISIBLE
        } else {
            binding.group.visibility = View.VISIBLE
        }
    }

    private fun clearSum() {
        model!!.clear()
        binding.balanceLeft.text = balanceLeft!!.getContext()
            .getString(R.string.balance_amount, 0.0)
        binding.group.visibility = View.INVISIBLE
    }

    private fun getProfile() {
        try {
            Fuel.get(
                ProfileURL
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@HomeActivity,
                                String(response.data),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        val jsonObject = JSONObject(result.get().content)
                        binding.name.text = jsonObject.getString("name")
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@HomeActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun getDrinks() {
        try {
            Fuel.get(
                DrinksURL
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@HomeActivity,
                                String(response.data),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        val jsonObject = JSONObject(result.get().content)
                        val jsonDrinks = jsonObject.getJSONArray("boissons")

                        for (i in 0 until jsonDrinks.length()) {
                            val item = JSONObject(jsonDrinks.getString(i))
                            val d = Drink(
                                item.getInt("id"),
                                item.getString("name"),
                                item.getString("price").toFloat(),
                                1,
                                item.getString("bgColor"),
                                item.getString("textColor")
                            )
                            adapter.addItem(d)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@HomeActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun getUserBalance() {
        try {
            Fuel.get(
                CustomerProfile
            ).authentication().bearer(binding.etToken.text.toString())
                .responseJson { request, response, result ->
                    when (result) {
                        is Result.Failure -> {
                            when (response.statusCode) {
                                401 -> Toast.makeText(
                                    this@HomeActivity,
                                    "Token non valide.",
                                    Toast.LENGTH_SHORT
                                ).show()
                                else -> Toast.makeText(
                                    this@HomeActivity,
                                    String(response.data),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                        is Result.Success -> {
                            val jsonObject = JSONObject(result.get().content)
                            solde = jsonObject.getDouble("balance")
                            userId = jsonObject.getString("id")
                            binding.solde.text = binding.solde.getContext()
                                .getString(R.string.balance_amount, jsonObject.getDouble("balance"))
                        }
                    }
                }
        } catch (e: Exception) {
            Toast.makeText(
                this@HomeActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun pay() {
        try {
            BalanceURL = "https://api.move-it.tech/user/{id}/balance"
            BalanceURL = BalanceURL.replace("{id}", userId)
            Fuel.put(
                BalanceURL,
                listOf("amount" to model!!.getTotal(), "boissons" to model!!.getAll().toString(), "tokenUser" to binding.etToken.text.toString())
            ).authentication().bearer(token)
                .responseJson { request, response, result ->
                    when (result) {
                        is Result.Failure -> {
                            when (response.statusCode) {
                                400 -> Toast.makeText(
                                    this@HomeActivity,
                                    "Solde insuffisant",
                                    Toast.LENGTH_SHORT
                                ).show()
                                401 -> Toast.makeText(
                                    this@HomeActivity,
                                    "Token non valide",
                                    Toast.LENGTH_SHORT
                                ).show()
                                404 -> Toast.makeText(
                                    this@HomeActivity,
                                    "Utilisateur non trouvé",
                                    Toast.LENGTH_SHORT
                                ).show()
                                422 -> Toast.makeText(
                                    this@HomeActivity,
                                    "Requête invalide",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                        is Result.Success -> {
                            Toast.makeText(
                                this@HomeActivity,
                                "Payment OK.",
                                Toast.LENGTH_SHORT
                            ).show()
                            getUserBalance()
                            clearSum()
                        }
                    }
                }
        } catch (e: Exception) {
            Toast.makeText(
                this@HomeActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
