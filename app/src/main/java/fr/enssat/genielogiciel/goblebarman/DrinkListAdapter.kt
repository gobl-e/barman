package fr.enssat.genielogiciel.goblebarman

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeAdapter
import fr.enssat.genielogiciel.goblebarman.data.model.Drink

/**
 * Adapter for a list of drinks.
 */
class DrinkListAdapter(dataSet: List<Drink> = emptyList(), private val addToSumListener: (Drink) -> Unit) :
    DragDropSwipeAdapter<Drink, DrinkListAdapter.ViewHolder>(dataSet) {

    class ViewHolder(DrinksLayout: View) : DragDropSwipeAdapter.ViewHolder(DrinksLayout) {
        val DrinksNameView: TextView = itemView.findViewById(R.id.drinks_name)
        val DrinksPriceView: TextView = itemView.findViewById(R.id.drinks_price)
        val EuroView: TextView = itemView.findViewById(R.id.euro)
        val layout: ConstraintLayout = itemView.findViewById(R.id.layoutGrid)
        val dragIcon: AppCompatImageView = itemView.findViewById(R.id.drag_icon)
    }

    override fun getViewHolder(itemView: View): ViewHolder {
        return ViewHolder(
            itemView
        )
    }

    override fun onBindViewHolder(item: Drink, viewHolder: ViewHolder, position: Int) {
        val context = viewHolder.itemView.context

        val bgColor = Color.parseColor('#'+item.bgColor)
        val textColor = Color.parseColor('#'+item.textColor)

        viewHolder.DrinksNameView.text = item.name
        viewHolder.DrinksPriceView.text = context.getString(R.string.priceFormat, item.price)
        viewHolder.layout.setBackgroundColor(bgColor)
        viewHolder.DrinksNameView.setTextColor(textColor)
        viewHolder.DrinksPriceView.setTextColor(textColor)
        viewHolder.EuroView.setTextColor(textColor)
        viewHolder.layout.setOnClickListener {
            addToSumListener(item)
        }
    }

    override fun getViewToTouchToStartDraggingItem(
        item: Drink,
        viewHolder: ViewHolder,
        position: Int
    ) = viewHolder.dragIcon
}