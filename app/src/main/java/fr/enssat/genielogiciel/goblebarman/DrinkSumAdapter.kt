package fr.enssat.genielogiciel.goblebarman

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.genielogiciel.goblebarman.data.model.Drink


class DrinkSumViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    val countTextView = view.findViewById(R.id.countDrink) as TextView
    val nameTextView = view.findViewById(R.id.drink) as TextView
    val priceTextView = view.findViewById(R.id.price) as TextView
    val btnDelete = view.findViewById(R.id.btnDelete) as Button

    fun setDrink(drink: Drink, delete: (Drink) -> Unit) {
        nameTextView.text = drink.name
        countTextView.text = drink.count.toString()
        priceTextView.text = (drink.count * drink.price).toString()

        btnDelete.setOnClickListener {
            delete(drink)
        }

    }
}

class DrinkSumAdapter(
    private val deleteListener: (Drink) -> Unit
) :
    RecyclerView.Adapter<DrinkSumViewHolder>() {

    var list: List<Drink> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return this.list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkSumViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.sum_item_list, parent, false)
        return DrinkSumViewHolder(view)
    }

    override fun onBindViewHolder(holder: DrinkSumViewHolder, position: Int) {
        holder.setDrink(this.list[position], deleteListener)
    }
}