package fr.enssat.genielogiciel.goblebarman

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import org.json.JSONException
import org.json.JSONObject

class Utils {
    companion object {
        fun getErrorMessage(response: String): String {
            try {
                val jsonObject = JSONObject(response)
                return jsonObject.getString("message")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return "No data"
        }

        fun Fragment.hideKeyboard() {
            view?.let { activity?.hideKeyboard(it) }
        }

        fun Activity.hideKeyboard() {
            hideKeyboard(currentFocus ?: View(this))
        }

        fun Context.hideKeyboard(view: View) {
            val inputMethodManager =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}