package fr.enssat.genielogiciel.goblebarman

import android.content.Context
import android.content.SharedPreferences

class PreferenceHelper(private val context: Context) {

    private val status = "status"
    private val token: String = "token"
    private val appPrefs: SharedPreferences = context.getSharedPreferences(
        "shared",
        Context.MODE_PRIVATE
    )

    fun putIsLogin(loginorout: Boolean) {
        val edit = appPrefs.edit()
        edit.putBoolean(status, loginorout)
        edit.apply()
    }

    fun getIsLogin(): Boolean {
        return appPrefs.getBoolean(status, false)
    }

    fun putToken(loginorout: String) {
        val edit = appPrefs.edit()
        edit.putString(token, loginorout)
        edit.apply()
    }

    fun getToken(): String? {
        return appPrefs.getString(token, "")
    }
}