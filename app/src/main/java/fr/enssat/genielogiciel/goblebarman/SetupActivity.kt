package fr.enssat.genielogiciel.goblebarman

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import fr.enssat.genielogiciel.goblebarman.Utils.Companion.hideKeyboard
import fr.enssat.genielogiciel.goblebarman.data.model.Drink
import fr.enssat.genielogiciel.goblebarman.data.model.DrinkViewModel
import fr.enssat.genielogiciel.goblebarman.databinding.ActivitySetupBinding
import org.json.JSONObject


class SetupActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySetupBinding
    private var DrinksURL = "https://api.move-it.tech/boisson"
    private var preferenceHelper: PreferenceHelper? = null
    private var token: String = ""
    private var adapter: DrinkSetupAdapter = DrinkSetupAdapter(this::save, this::delete)
    private var model: DrinkViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferenceHelper = PreferenceHelper(this)
        token = preferenceHelper!!.getToken() ?: ""

        if (token == "") {
            logout(true)
        }

        val actionBar: ActionBar? = supportActionBar
        actionBar!!.hide()

        binding = DataBindingUtil.setContentView(this, R.layout.activity_setup)
        model = ViewModelProvider(this).get(DrinkViewModel::class.java)

        val itemDecor = DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL)
        itemDecor.setDrawable(applicationContext.getResources().getDrawable(R.drawable.divider))
        binding.drinkList.addItemDecoration(itemDecor)

        binding.drinkList.adapter = adapter

        model!!.drinks.observe(this, Observer<List<Drink>> { list ->
            adapter.list = list
        })

        getDrinks()

        binding.btnBack.setOnClickListener {
            val intent = Intent(this@SetupActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }

        binding.btnAddDrink.setOnClickListener {
            add()
        }

        binding.swipeRefresh.setOnRefreshListener {
            getDrinks()
        }

    }

    private fun logout(auto: Boolean) {
        if (auto) {
            Toast.makeText(
                this@SetupActivity,
                "Your session has expired, please login again.",
                Toast.LENGTH_SHORT
            ).show()
        }
        preferenceHelper!!.putIsLogin(false)
        val intent = Intent(this@SetupActivity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        this@SetupActivity.finish()
    }

    private fun getDrinks() {
        try {
            Fuel.get(
                DrinksURL
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@SetupActivity,
                                String(response.data),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        val jsonObject = JSONObject(result.get().content)
                        val jsonDrinks = jsonObject.getJSONArray("boissons")
                        val drinks = arrayListOf<Drink>()

                        for (i in 0 until jsonDrinks.length()) {
                            val item = JSONObject(jsonDrinks.getString(i))
                            val d = Drink(
                                item.getInt("id"),
                                item.getString("name"),
                                item.getString("price").toFloat(),
                                1,
                                item.getString("bgColor"),
                                item.getString("textColor")
                            )
                            drinks.add(d)
                        }
                        updateUI(drinks)
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@SetupActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun updateUI(drinks: List<Drink>) {
        binding.swipeRefresh.isRefreshing = false
        model!!.clear()
        model!!.addList(drinks)
        adapter.notifyDataSetChanged()
    }

    private fun add() {
        try {
            Fuel.post(
                DrinksURL, listOf(
                    "name" to "Nouvelle boisson"
                    , "price" to 0.0
                )
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@SetupActivity,
                                String(response.data),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        getDrinks()
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@SetupActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun save(d: Drink){
        try {
            Fuel.put(
                DrinksURL + '/' + d.id, listOf(
                    "name" to d.name
                    , "price" to d.price
                    , "bgColor" to d.bgColor
                    , "textColor" to d.textColor
                )
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@SetupActivity,
                                String(response.data),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        getDrinks()
                        Toast.makeText(
                            this@SetupActivity,
                            "Modification sauvegardée.",
                            Toast.LENGTH_SHORT
                        ).show()
                        hideKeyboard()
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@SetupActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun delete(d: Drink) {
        try {
            Fuel.delete(
                DrinksURL + '/' + d.id
            ).authentication().bearer(token).responseJson { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        when (response.statusCode) {
                            401 -> logout(true)
                            else -> Toast.makeText(
                                this@SetupActivity,
                                String(response.data),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    is Result.Success -> {
                        model!!.remove(d)
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                this@SetupActivity,
                "Une erreur est survenue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
