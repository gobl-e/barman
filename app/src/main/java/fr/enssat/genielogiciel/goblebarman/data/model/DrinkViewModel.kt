package fr.enssat.genielogiciel.goblebarman.data.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONArray
import org.json.JSONObject


class DrinkViewModel : ViewModel() {

    private val _drinkSet = mutableListOf<Drink>()
    private val _alldrinks = MutableLiveData<List<Drink>>()

    val drinks: LiveData<List<Drink>> get() = _alldrinks

    fun clear() {
        _drinkSet.clear()
        _alldrinks.postValue(_drinkSet.toList())
    }

    fun addList(drinks: List<Drink>) {
        _drinkSet.addAll(drinks)
        _alldrinks.postValue(_drinkSet.toList())
    }

    fun add(d: Drink) {
        if (_drinkSet.contains(d)) {
            val i = _drinkSet.indexOf(d)
            _drinkSet.remove(d)
            d.count += 1
            _drinkSet.add(i, d)
        } else {
            d.count = 1
            _drinkSet.add(d)
        }
        _alldrinks.postValue(_drinkSet.toList())
    }

    fun remove(d: Drink) {
        _drinkSet.remove(d)
        _alldrinks.postValue(_drinkSet.toList())
    }

    fun removeSum(d: Drink) {
        val i = _drinkSet.indexOf(d)
        _drinkSet.remove(d)
        if (d.count > 1) {
            d.count -= 1
            _drinkSet.add(i, d)
        }
        _alldrinks.postValue(_drinkSet.toList())
    }

    fun isEmpty(): Boolean {
        return _drinkSet.isEmpty()
    }

    fun getTotal(): Float {
        var total = 0.0F

        _drinkSet.forEach {
            total += (it.count * it.price)
        }

        return total
    }

    fun getAll(): JSONArray {
        val array = JSONArray()

        _drinkSet.forEach {
            val o = JSONObject()
            o.put("id", it.id)
            o.put("count", it.count)
            array.put(o)
        }

        return array
    }
}