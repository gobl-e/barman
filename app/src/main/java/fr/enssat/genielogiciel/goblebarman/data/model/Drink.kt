package fr.enssat.genielogiciel.goblebarman.data.model

class Drink(
    val id: Int,
    var name: String,
    var price: Float,
    var count: Int,
    var bgColor: String,
    var textColor: String
) {

    override fun toString() = name
}