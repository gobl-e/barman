package fr.enssat.genielogiciel.goblebarman

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.genielogiciel.goblebarman.data.model.Drink


class DrinkViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    private val drinkView: EditText = view.findViewById(R.id.drink)
    private val priceView: EditText = view.findViewById(R.id.price)
    private val bgColorView: EditText = view.findViewById(R.id.bgColor)
    private val textColorView: EditText = view.findViewById(R.id.textColor)
    private val btnSave: Button = view.findViewById(R.id.btnSave)
    private val btnDelete: Button = view.findViewById(R.id.btnDelete)

    fun setDrink(drink: Drink, save: (Drink) -> Unit, delete: (Drink) -> Unit) {
        drinkView.setText(drinkView.getContext().getString(R.string.drinkname, drink.name))
        priceView.setText(priceView.getContext().getString(R.string.priceFormat, drink.price))
        bgColorView.setText(bgColorView.getContext().getString(R.string.drinkname, drink.bgColor))
        textColorView.setText(textColorView.getContext().getString(R.string.drinkname, drink.textColor))

        btnSave.setOnClickListener {
            drink.name = drinkView.text.toString()
            drink.price = priceView.text.toString().toFloat()
            drink.bgColor = bgColorView.text.toString()
            drink.textColor = textColorView.text.toString()
            save(drink)
        }

        btnDelete.setOnClickListener {
            delete(drink)
        }

    }
}

class DrinkSetupAdapter(
    private val saveListener: (Drink) -> Unit,
    private val deleteListener: (Drink) -> Unit
) :
    RecyclerView.Adapter<DrinkViewHolder>() {

    var list: List<Drink> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return this.list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_drink, parent, false)
        return DrinkViewHolder(view)
    }

    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        holder.setDrink(this.list[position], saveListener, deleteListener)
    }
}